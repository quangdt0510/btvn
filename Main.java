package demoOOP;

import com.sun.istack.internal.localization.NullLocalizable;
import demoOOP.Teacher;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Teacher John= new Teacher("John",25,true,LocalDate.of(1992,1,1),
                new ArrayList<>(Arrays.asList("Java", "SQL")));
        System.out.print(John);
        Teacher Peter = new Teacher();
        Peter.setAge(38);
        System.out.print(Peter);
        Test.Compare(John.getAge(),Peter.getAge());
    }



}
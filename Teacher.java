package demoOOP;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Teacher {
        private String fullName;
        private int age;
    private boolean marrie;
    private LocalDate birthDay;
    private List<String> major;


    public void eat(){

    }
    public void learning(){

    }
    public void teach(){

    }

    public Teacher(){

    }
    public Teacher(String fullName, int age, boolean marrie, LocalDate birthDay, List<String> major) {
        this.fullName = fullName;
        this.age = age;
        this.marrie = marrie;
        this.birthDay = birthDay;
        this.major = major;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getMarrie() {
        return marrie;
    }

    public void setMarrie(boolean marrie) {
        this.marrie = marrie;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public List<String> getMajor() {
        return major;
    }

    public void setMajor(List<String> major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "fullName='" + fullName + '\'' +
                ", age=" + age +
                ", marrie='" + marrie + '\'' +
                ", birthDay=" + birthDay +
                ", major=" + major +
                '}';
    }
}
